﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RulesDesignPattern
{
    public class User
    {
        public bool WasReferred { get; set; }
        public int Age { get; set; }
        public bool IsMember { get; set; }
        public bool IsFirstPurchase { get; set; }

    }
}
