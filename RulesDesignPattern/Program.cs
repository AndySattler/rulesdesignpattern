﻿using System;
using System.Collections.Generic;
using System.Linq;
using RulesDesignPattern.Rules;

namespace RulesDesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            User user = new User() { 
                IsMember = true,
                Age = 45,
                IsFirstPurchase = false,
                WasReferred = false
            };

            List<IRule> rules = new List<IRule>()
            {
                new MemberDiscount(),
                new MemberFirstPurchaseDiscount(),
                new ReferredDiscount(),
                new SeniorDiscount()
            };

            decimal discount = rules.Select(p=>p.GetDiscount(user))
                                    .Max();

            Console.WriteLine($"Discount is {discount*100}%");
        }
    }
}
