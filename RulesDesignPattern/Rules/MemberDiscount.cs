﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RulesDesignPattern.Rules
{
    public class MemberDiscount : IRule
    {
        public decimal GetDiscount(User user)
        {
            return user.IsMember ? .12m : 0;
        }
    }
}
