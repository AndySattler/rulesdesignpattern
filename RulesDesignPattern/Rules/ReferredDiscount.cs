﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RulesDesignPattern.Rules
{
    public class ReferredDiscount : IRule
    {
        public decimal GetDiscount(User user)
        {
            return user.WasReferred ? .05m : 0;
        }
    }
}
