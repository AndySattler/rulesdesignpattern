﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RulesDesignPattern.Rules
{
    interface IRule
    {
        decimal GetDiscount(User user);
    }
}
