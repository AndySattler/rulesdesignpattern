﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RulesDesignPattern.Rules
{
    public class MemberFirstPurchaseDiscount : IRule
    {
        public decimal GetDiscount(User user)
        {
            return user.IsMember && user.IsFirstPurchase ? .25m : 0;
        }
    }
}
