﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RulesDesignPattern.Rules
{
    public class SeniorDiscount : IRule
    {
        public decimal GetDiscount(User user)
        {
            return user.Age >= 55 ? .10m : 0;
        }
    }
}
